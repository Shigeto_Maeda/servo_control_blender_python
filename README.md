# README #

## An experiment project of communication between Blender and Arduino.##

### Files ###
* _2servo(Folder) : Arduino sketch file for first experiment of communication between Blender and Arduino.

* servoModel_rotate_servo.blend : Blender file, 2 servo objects moving with curve control.

* servoModel_track_target.blend : Blender file, 2 servo objects track a 'Target'.

* control_2servo_blender_python.py :  The python code for 'servoModel_rotate_servo.blend'. Sending 2 values of rotation angles to Arduino with serial communication.

* blender_to_ardSketch_servoVer.py : The python code for 'servoModel_rotate_servo.blend'. Sending 2 values of rotation angles to Arduino with serial communication. The rotation data is gotten from object's angle. Finally, write to .ino file on desktop.

* blender_to_ardSketch_targetVer.py :  The python code for 'servoModel_track_target.blend'. Sending 2 values of rotation angles to Arduino with serial communication. The rotation data is gotten from object's Matrix data. Finally, write to .ino file on desktop.

### Usage ###
Connect the Arduino with an USB cable. Connect the 'ServoA' data line to #9 pin, the 'ServoB' data line to #10 on the Arduino. Open the '_2servo' from Arduino IDE, and write it.

PySerial is required. download and install PySerial from 'https://pypi.python.org/pypi/pyserial'. For using Mac, right click the blender.app, see in the package > Contents > Resources > 2.74 > python > lib > python3.4 > some-packages folder. Put the 'serial' folder  in to it from the unzipped pyserial folder.

Open the .blend file, load the python code in the 'Text Editor' window, push 'Run Script', and Alt 'A' to start animation.