#include <Servo.h> 

Servo A;
Servo B; 

void setup() {

  A.attach(9);
  B.attach(10);
  Serial.begin(9600);
  A.write(90); 
  B.write(90); 

} 

int num; 
int angle; 

void loop() {   

  while(Serial.available() == 0);
  num = Serial.read();
  while(Serial.available() == 0);
  angle = Serial.read();
  if (num == 9)
    A.write(angle);
  else if (num == 10)
    B.write(angle); 
  
}

