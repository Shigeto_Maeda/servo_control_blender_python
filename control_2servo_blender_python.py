import bpy
import math
from math import degrees
import serial
# Create a persistent connection to COM port 6 (Win) /dev/tty.~(Mac)

ser = serial.Serial('/dev/tty.usbmodemfa1411',9600,timeout=1) 
      
def my_handler(scene):
    """Handler that get's the Euler X rotation from two cubes and passes this to servos 9 and 10 though the serial port.""" 
    # Get the Euler x rotation in degrees.  The rotation for "Cube" will be applied to servo on pin 9   

    servoAngle9 = degrees(bpy.data.objects['ServoA'].rotation_euler.z)   
    # Get the Euler x rotation in degrees.  The rotation for "Cube1" will be applied to the servo on pin 10     
    servoAngle10 = degrees(bpy.data.objects['ServoB'].rotation_euler.z) 
    # Convert these values into a byte array.   
    data = bytes( [9, int(servoAngle9), 10, int(servoAngle10)] )   
    # Write the commands to the serial port.   
    ser.write(data) 
           
# Register the handler to be called once the frame has changed. 
bpy.app.handlers.frame_change_post.append(my_handler)
