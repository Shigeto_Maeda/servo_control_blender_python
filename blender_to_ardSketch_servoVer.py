import bpy
import math
from math import degrees
import serial
import os
# Create a persistent connection to COM port 6 (Win) /dev/tty.~(Mac)

myDataA = []
myDataB = []

scene = bpy.context.scene
intvlTime = 30
ser = serial.Serial('/dev/tty.usbmodemfa1411',9600,timeout=1) 

def output2sketch(a, b, endFrame, intvlTime): # write 'ARDUINO' sketch from blender python!
	os.mkdir("/users/shige/desktop/bl2sketch")
	f = open("/users/shige/desktop/bl2sketch/bl2sketch.ino", "w")
		
	f.write('\x23include<Servo.h>\n')
	f.write('\n')
	f.write('Servo A;\n')
	f.write('Servo B;\n')
	f.write('\n')
	f.write('int angleA;\n')
	f.write('int angleB;\n')
	
	f.write('int servoA_angle[] = {')
	for i in range(endFrame-1):
		ang = str(a[i])
		f.write(ang)
		if (i < endFrame-2):
			f.write(', ')
		else:
			f.write('};\n')
			
	f.write('int servoB_angle[] = {')
	for i in range(endFrame-1):
		ang = str(b[i])
		f.write(ang)
		if (i < endFrame-2):
			f.write(', ')
		else:
			f.write('};\n')	
	
	f.write('int interval = ')
	f.write(str(intvlTime))
	f.write(';\n')
	f.write('\n')
	
	f.write('void setup(){\n')
	f.write('\n')
	f.write('    A.attach(9);\n')
	f.write('    B.attach(10);\n')
	f.write('    A.write(90);\n')
	f.write('    B.write(90);\n')
	f.write('\n')
	f.write('}\n')
	f.write('\n')
	
	f.write('void loop(){\n')
	f.write('\n')
	f.write('    for (int i = 0; i < ')
	f.write(str(endFrame - 1))
	f.write('; i++){\n')
	f.write('        angleA = servoA_angle[i];\n')
	f.write('        angleB = servoB_angle[i];\n')	
	f.write('        A.write(angleA);\n')
	f.write('        B.write(angleB);\n')
	f.write('        delay(interval);\n')
	f.write('\n')
	f.write('    }\n')
	f.write('}\n')
	
	f.close()

def stop_anim(scene): # Stop animation with end of animation (no loop)
    if scene.frame_current == scene.frame_end:  # >=  may be better.
        print("cancel")
        bpy.ops.screen.animation_cancel(restore_frame=True)
        
def my_handler(scene):
    """Handler that get's the Euler Z rotation from two cubes and passes this to servos 9 and 10 though the serial port.""" 
    # Get the Euler z rotation in degrees.  The rotation for "ServoA" will be applied to servo on pin 9   
    servoAngle9 = degrees(bpy.data.objects['ServoA'].rotation_euler.z)   
    # Get the Euler z rotation in degrees.  The rotation for "ServoB" will be applied to the servo on pin 10     
    servoAngle10 = degrees(bpy.data.objects['ServoB'].rotation_euler.z) 
    # Convert these values into a byte array.   
    data = bytes( [9, int(servoAngle9), 10, int(servoAngle10)] )   
    # Write the commands to the serial port.   
    ser.write(data) 
    
    currentFrame = bpy.context.scene.frame_current
    endFrame = bpy.context.scene.frame_end
    #print(currentFrame, int(servoAngle9), int(servoAngle10))
    
    myDataA.append(int(servoAngle9))
    myDataB.append(int(servoAngle10))
    
    if(currentFrame == endFrame):
     
        output2sketch(myDataA, myDataB, endFrame, intvlTime)
        
        stop_anim(scene)
       
# Register the handler to be called once the frame has changed. 
bpy.app.handlers.frame_change_post.append(my_handler)
